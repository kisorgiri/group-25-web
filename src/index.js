import React from 'react';
import ReactDOM from 'react-dom';
import RootComponent from './components/app.component';


ReactDOM.render(<RootComponent></RootComponent>, document.getElementById('root'))


// component
// component is basic building block of react
// component can be written using function as well as class
// incoming data inside a component are called props
// data within a component are called state
// component can be stateful, or stateless
// for our overal course we will use functional component as stateless
// and class based component for stateful