import httpClient from "../../util/httpClient";
import notify from "../../util/notificiation";

export const ProductActionTypes = {
    SET_IS_LOADING: 'SET_IS_LOADING',
    PRODUCT_RECIVED: 'PRODUCT_RECEIVED',
    PRODUCT_REMOVED: 'PRODUCT_REMOVED'
}

export const fetch_products_ac = (reqParams) => {
    return (dispatch) => {
        console.log('i am at actions');
        console.log("i will dispatch action to reducer")
        dispatch({
            type: ProductActionTypes.SET_IS_LOADING,
            payload: true
        })
        httpClient.GET('/product', true)
            .then(response => {
                dispatch({
                    type: ProductActionTypes.PRODUCT_RECIVED,
                    payload: response.data
                })
            })
            .catch(err => {
                notify.handleError(err);
            })
            .finally(() => {
                dispatch({
                    type: ProductActionTypes.SET_IS_LOADING,
                    payload: false
                })
            })
    }
}

export const remvoe_product_ac = (id) => (dispatch, getState) => {
    console.log('state >>', getState());
    const { product } = getState();
    const { products } = product;
    httpClient.DELETE(`/product/${id}`, true)
        .then(response => {
            notify.showInfo('Product Deleted')
            products.forEach((product, i) => {
                if (product._id === id) {
                    products.splice(i, 0);
                }
            })
            dispatch({
                type: ProductActionTypes.PRODUCT_REMOVED,
                payload: products
            })
        })
        .catch(err => {
            notify.handleError(err);
        })
}
