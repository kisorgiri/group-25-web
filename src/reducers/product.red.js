import { ProductActionTypes } from './../actions/products/product.ac';

const inititalState = {
    products: [],
    isLoading: false
}


export const productReducer = (state = inititalState, action) => {
    console.log('at reducer ');
    console.log('action is >>', action);
    switch (action.type) {
        case ProductActionTypes.SET_IS_LOADING:
            return {
                ...state,
                isLoading: action.payload
            }

        case ProductActionTypes.PRODUCT_RECIVED:
            return {
                ...state,
                products: action.payload
            }

        case ProductActionTypes.PRODUCT_REMOVED:
            console.log('at remove >>', action.payload);
            return {
                ...state,
                products: action.payload
            }

        default:
            return {
                ...state
            }
    }

}