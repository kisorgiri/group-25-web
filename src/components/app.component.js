import React from 'react';
import AppRoute from './app.routing';
import { ToastContainer } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';
import { Provider } from 'react-redux';

import { store } from './../store';
const App = (props) => {
    // functional component
    // props or a placeholder is incoming data to component
    // it must return a single html node
    // this component can be used as an element
    // props are those value whicha re sent using as attribute to our own element

    return (
        <>
            <Provider store={store}>
                <AppRoute></AppRoute>
            </Provider>
            <ToastContainer />
        </>
    )
}

export default App