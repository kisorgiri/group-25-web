import React from 'react';
import { Link } from 'react-router-dom';
import notify from './../../../util/notificiation';
import httpClient from './../../../util/httpClient';

export class Login extends React.Component { //extends is used fro inheritance

    constructor() {
        super();
        this.state = {
            data: {
                username: '',
                password: ''
            },
            error: {
                username: '',
                password: ''
            },
            remember_me: false,
            isSubmitting: false,
            isValidForm: false
        };
    }
    componentDidMount() {
        if (localStorage.getItem('remember_me') === 'true') {
            this.props.history.push('/dashboard');
        }
    }


    handleChange = (e) => {
        let { type, name, value, checked } = e.target
        if (type === 'checkbox') {
            return this.setState({
                [name]: checked
            })
        }
        this.setState((preState) => ({
            data: {
                ...preState.data,
                [name]: value
            }
        }), () => {
            this.validateForm(name);
        });
    }
    validateForm(filedName) {
        let errMsg = this.state.data[filedName]
            ? ''
            : `${filedName} is Required`

        this.setState((preState) => ({
            error: {
                ...preState.error,
                [filedName]: errMsg
            }
        }), () => {
            const errors = Object
                .values(this.state.error)
                .filter(err => err);
            this.setState({
                isValidForm: errors.length === 0
            })
        })
    }

    handleSubmit = (e) => {
        e.preventDefault();
        this.setState({
            isSubmitting: true
        })
        httpClient.POST('/auth/login', this.state.data, {})
            .then(response => {
                notify.showSuccess('Welcome ' + response.data.user.username);
                localStorage.setItem('token', response.data.token)
                localStorage.setItem('user', JSON.stringify(response.data.user));
                localStorage.setItem('remember_me', this.state.remember_me);
                this.props.history.push('/dashboard');
            })
            .catch(error => {
                notify.handleError(error);
                this.setState({
                    isSubmitting: false
                })
            })


    }
    render() {
        return <div>
            <h2>Login</h2>
            <p>Please login to start your session</p>
            <form className="form-group" onSubmit={this.handleSubmit}>
                <label htmlFor="username">Username</label>
                <input className="form-control" type="text" placeholder="Username" name="username" id="username" onChange={this.handleChange}></input>
                <p className="error">{this.state.error.username}</p>
                <label htmlFor="password">Password</label>
                <input className="form-control" type="password" placeholder="Password" name="password" id="password" onChange={this.handleChange}></input>
                <p className="error">{this.state.error.password}</p>
                <input type="checkbox" name="remember_me" onChange={this.handleChange}></input>
                <label>Remeber Me</label>
                <br />
                <button disabled={this.state.isSubmitting || !this.state.isValidForm} type="submit" className="btn btn-primary m_t_10">
                    {
                        this.state.isSubmitting
                            ? 'logging in....'
                            : 'login'
                    }
                </button>
            </form>
            <p>Don't have an Account?</p>
            <p>
                <span>Register <Link to="/register">here</Link></span>
                <Link to="/forgot_password" className="float_right">forgot password?</Link>
            </p>

        </div>
    }
}