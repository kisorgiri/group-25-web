import React, { Component } from 'react'
import httpClient from '../../../util/httpClient';
import notificiation from '../../../util/notificiation';

export default class ForgotPassword extends Component {

    constructor() {
        super();
        this.state = {
            email: '',
            isSubmitting: false
        };
    }

    handleChange = e => {
        const { name, value } = e.target;
        this.setState({
            [name]: value
        })
    }

    handleSubmit = e => {
        this.setState({
            isSubmitting: true
        })
        e.preventDefault();
        httpClient.POST('/auth/forgot-password', { email: this.state.email })
            .then(response => {
                this.props.history.push('/');
                notificiation.showInfo("Password reset link sent to your email please check your inbox");
            })
            .catch(err => {
                notificiation.handleError(err);
                this.setState({
                    isSubmitting: false
                })
            })
    }
    render() {
        let btn = this.state.isSubmitting
            ? <button disabled className="btn btn-info">submitting...</button>
            : <button className="btn btn-primary" type="submit">submit</button>
        return (
            <>
                <h2>Forgot Password</h2>
                <p>Please provide your email address to reset your password</p>
                <form className="form-group" onSubmit={this.handleSubmit} noValidate>
                    <label>Email</label>
                    <input type="text" className="form-control" name="email" placeholder="Email Address" onChange={this.handleChange} ></input>
                    <br />
                    {btn}
                </form>

            </>
        )
    }
}
