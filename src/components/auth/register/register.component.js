import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import notify from './../../../util/notificiation';
import httpClient from './../../../util/httpClient';

const defaultFrom = {
    name: '',
    email: '',
    phoneNumber: '',
    username: '',
    password: '',
    gender: '',
    dob: ''
}
export class RegisterComponent extends Component {

    constructor() {
        super();
        this.state = {
            data: {
                ...defaultFrom
            },
            error: {
                ...defaultFrom
            },
            isSubmitting: false,
            isValidFrom: false
        };
    }

    handleChange = (e) => {
        const { name, value } = e.target;
        this.setState(preState => ({
            data: {
                ...preState.data,
                [name]: value
            }
        }), () => {
            this.validateFrom(name);
        })
    }

    validateFrom(fieldName) {
        let errMsg;
        switch (fieldName) {
            case 'username':
                errMsg = this.state.data[fieldName]
                    ? ''
                    : 'Required Field*'
                break;
            case 'password':
                errMsg = this.state.data[fieldName]
                    ? this.state.data[fieldName].length > 6
                        ? ''
                        : 'Weak Password'
                    : 'Required Field*';
                break;

            case 'email':
                errMsg = this.state.data[fieldName]
                    ? this.state.data[fieldName].includes('@') && this.state.data[fieldName].includes('.com')
                        ? ''
                        : 'Invalid Email'
                    : 'Required Field*'
                break;
            default:
                break;
        }
        this.setState(preState => ({
            error: {
                ...preState.error,
                [fieldName]: errMsg
            }
        }), () => {
            const errors = Object
                .values(this.state.error)
                .filter(item => item)

            this.setState({
                isValidFrom: errors.length === 0
            })
        })

    }

    handleSubmit = (e) => {
        e.preventDefault();
        // API call
        this.setState({
            isSubmitting: true
        })
        httpClient.POST('/auth/register', this.state.data, {})
            .then((response) => {
                notify.showSuccess("Registration Successfull please login!")
                // show message
                this.props.history.push('/'); //navigate
            })
            .catch(err => {
                // error handling
                notify.handleError(err);
                this.setState({
                    isSubmitting: false
                })
            })
    }


    render() {
        let btn = this.state.isSubmitting
            ? <button disabled className="btn btn-info">submitting...</button>
            : <button disabled={!this.state.isValidFrom} className="btn btn-primary" type="submit">submit</button>
        return (
            <div>
                <h2>Register</h2>
                <p>Please provide necessary details to register</p>
                <form className="form-group" onSubmit={this.handleSubmit} noValidate>
                    <label>Name</label>
                    <input type="text" name="name" className="form-control" placeholder="Name" onChange={this.handleChange}></input>
                    <label>Email</label>
                    <input type="text" name="email" className="form-control" placeholder="Email" onChange={this.handleChange}></input>
                    <p className="error">{this.state.error.email}</p>
                    <label>Username</label>
                    <input type="text" name="username" className="form-control" placeholder="Username" onChange={this.handleChange}></input>
                    <p className="error">{this.state.error.username}</p>
                    <label>Password</label>
                    <input type="password" name="password" className="form-control" placeholder="Password" onChange={this.handleChange}></input>
                    <p className="error">{this.state.error.password}</p>
                    <label>Gender</label>
                    <input type="text" name="gender" className="form-control" placeholder="Gender" onChange={this.handleChange}></input>
                    <label>Date Of Birth</label>
                    <input type="date" name="Date Of Birth" className="form-control" onChange={this.handleChange}></input>
                    <label>Temporary Address</label>
                    <input type="text" name="temp_address" className="form-control" placeholder="Temporary Address" onChange={this.handleChange}></input>
                    <label>Permanent Address</label>
                    <input type="text" name="permanent_address" className="form-control" placeholder="Permanent Address" onChange={this.handleChange}></input>
                    <br />
                    {btn}
                </form>
                <p>Already logged in?
                    <Link to="/">back to login</Link>
                </p>
            </div>
        )
    }
}