import React, { Component } from 'react'
import httpClient from '../../../util/httpClient';
import notificiation from '../../../util/notificiation';

export default class ResetPassword extends Component {

    constructor() {
        super();
        this.state = {
            password: '',
            confirmPassword: '',
            isSubmitting: false
        };
    }

    componentDidMount() {
        this.id = this.props.match.params['id'];
        console.log('this.id >>', this.id)
    }

    handleChange = e => {
        const { name, value } = e.target;
        this.setState({
            [name]: value
        })
        // TODO form validation 
    }

    handleSubmit = e => {
        e.preventDefault();
        this.setState({
            isSubmitting: true
        })

        httpClient.POST('/auth/reset-password/' + this.id, { password: this.state.password })
            .then(response => {
                this.props.history.push('/');
                notificiation.showInfo("Password reset successfull please login");
            })
            .catch(err => {
                notificiation.handleError(err);
                this.setState({
                    isSubmitting: false
                })
            })
    }
    render() {
        let btn = this.state.isSubmitting
            ? <button disabled className="btn btn-info">submitting...</button>
            : <button className="btn btn-primary" type="submit">submit</button>
        return (
            <>
                <h2>Reset Password</h2>
                <p>Please Choose your password wisely</p>
                <form className="form-group" onSubmit={this.handleSubmit} noValidate>
                    <label>Password</label>
                    <input type="password" className="form-control" name="password" placeholder="Password" onChange={this.handleChange} ></input>
                    <label>Confrim Password</label>
                    <input type="password" className="form-control" name="confirmPassword" placeholder="Confrim Password" onChange={this.handleChange} ></input>
                    <br />
                    {btn}
                </form>

            </>
        )
    }
}
