import React from 'react';
import './navbar.component.css';
import { Link, withRouter } from 'react-router-dom';

const logout = (history) => {
    // clear localstorage 
    localStorage.clear();
    history.push('/');

    // navigate to login page
}

const NavBarComponent = (props) => {
    let nav_bar = props.isLoggedIn
        ? <ul className="nav_list">
            <li className="nav_item">
                <Link to="/dashboard"> Home</Link>
            </li>
            <li className="nav_item">
                <Link to="/profile"> Profile</Link>
            </li>
            <li className="nav_item">
                <Link to="/change-password"> Change Password</Link>
            </li>

            <li className="logout">
                <button className="btn btn-success" onClick={() => logout(props.history)}>logout</button>
            </li>
        </ul>
        : <ul className="nav_list">
            <li className="nav_item">
                <Link to="/"> Login</Link>

            </li>
            <li className="nav_item">
                <Link to="/register"> Register</Link>

            </li>
        </ul>
    return (
            nav_bar
    )
}

export const NavBar = withRouter(NavBarComponent);