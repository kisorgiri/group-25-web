import React from 'react';
import { BrowserRouter as Router, Route, Switch, Redirect } from 'react-router-dom';
import { Login } from './auth/login/login.component';
import { RegisterComponent } from './auth/register/register.component';
import { NavBar } from './common/navbar/navbar.component';
import { SideBar } from './common/sidebar/sidebar.component';
import AddProduct from './products/addProduct/addProduct.component';
import ViewProduct from './products/viewProduct/viewProduct.component';
import EditProduct from './products/editProduct/editProduct.component';
import SearchProduct from './products/searchProduct/searchProduct.component';
import ForgotPassword from './auth/forgotPassword/forgotPassword.component';
import ResetPassword from './auth/resetPassword/resetPassword.component';
import ChatComponent from './user/chat/chat.component';

const Dashboard = () => {
    return <p>Please use Side Navigation menu or contact System Administrator for Support!</p>
}

const NotFound = () => {
    return <>
        <p>Page Not Found</p>
        <img src="./images/download.png" alt="404.png" width="400px"></img>
    </>
}

const ProtectedRoute = ({ component: Component, ...rest }) => {
    return <Route {...rest} render={routeProps => (
        localStorage.getItem('token')
            ? <div>
                <div className="nav_bar">
                    <NavBar isLoggedIn={true}></NavBar>
                </div>
                <div className="main">
                    <Component {...routeProps}></Component>
                </div>
                <div className="sidenav">
                    <SideBar></SideBar>
                </div>
            </div>
            :
            <Redirect to="/"></Redirect>
    )}>
    </Route>
}
const PublicRoute = ({ component: Component, ...rest }) => {
    return <Route {...rest} render={routeProps => (
        <div>
            <div className="nav_bar">
                <NavBar isLoggedIn={localStorage.getItem('token') ? true : false}></NavBar>
            </div>
            <div className="main">
                <Component {...routeProps}></Component>
            </div>
        </div>

    )}>
    </Route>
}


const AppRouting = (props) => {
    return (
        <Router>
            <Switch>
                <PublicRoute exact path="/" component={Login}></PublicRoute>
                <PublicRoute path="/register" component={RegisterComponent}></PublicRoute>
                <PublicRoute path="/forgot_password" component={ForgotPassword}></PublicRoute>
                <PublicRoute path="/reset-password/:id" component={ResetPassword}></PublicRoute>
                <ProtectedRoute path="/dashboard" component={Dashboard}></ProtectedRoute>
                <ProtectedRoute path="/view_product" component={ViewProduct}></ProtectedRoute>
                <ProtectedRoute path="/add_product" component={AddProduct} ></ProtectedRoute>
                <ProtectedRoute path="/edit_product/:id" component={EditProduct} ></ProtectedRoute>
                <ProtectedRoute path="/search_product" component={SearchProduct} ></ProtectedRoute>
                <ProtectedRoute path="/chat" component={ChatComponent} ></ProtectedRoute>
                <PublicRoute component={NotFound}></PublicRoute>
            </Switch>
        </Router>
    )
}

export default AppRouting;


// summarize
// react-router-dom
// wrapper==> browser Router
// config ==> route
// route config props ==> path, component,exact
// switch==> one path at a time
// Link==> for SPA, routing, to use link it must be inside a router warpper
// props=>  given by rote ==> history(for navigation),match(for dynamic endpoint value),location for quuery string