import React, { Component } from 'react'
import * as io from 'socket.io-client';
import './chat.component.css'
import Util from './../../../util/index';
import notify from './../../../util/notificiation';

export default class ChatComponent extends Component {

    constructor() {
        super();
        this.state = {
            data: {
                message: '',
                time: '',
                senderId: '',
                receiverId: '',
                senderName: '',
                receiverName: ''
            },
            currentUser: {},
            messages: [],
            users: []
        };
    }
    componentDidMount() {
        this.socket = io(process.env.REACT_APP_SOCKET_URL);
        this.runSocket();
        const currentUser = JSON.parse(localStorage.getItem('user'));
        this.socket.emit('new-user', currentUser.username);
        this.setState({
            currentUser
        })
    }

    runSocket() {
        this.socket.on('reply-msg-own', (data) => {
            const { messages } = this.state;
            messages.push(data);
            this.setState({
                messages
            })
        })
        this.socket.on('reply-msg-others', (data) => {
            this.setState(pre => ({
                data: {
                    ...pre.data,
                    receiverId: data.senderId
                }
            }))
            const { messages } = this.state;
            messages.push(data);
            this.setState({
                messages
            })
        })
        this.socket.on('users', (data) => {
            this.setState({
                users: data
            })
        })
    }
    handleChange = e => {
        const { name, value } = e.target;
        this.setState(pre => ({
            data: {
                ...pre.data,
                [name]: value
            }
        }))
    }

    selectUser(user) {
        this.setState(preState => ({
            data: {
                ...preState.data,
                receiverId: user.id,
                receiverName: user.name
            }
        }))
    }
    handleSubmit = e => {
        e.preventDefault();
        const { data, users, currentUser } = this.state;
        if (!data.receiverId) {
            return notify.showInfo('Please select a user to continue')
        }
        data.time = new Date();
        data.senderName = this.state.currentUser.username;
        data.senderId = users.find(item => item.name === currentUser.username).id;
        this.socket.emit('new-msg', data);
        this.setState((preState) => ({
            data: {
                ...preState.data,
                message: ''
            }
        }))
    }

    render() {
        return (
            <>
                <h2>Messages</h2> {this.state.currentUser.username}
                <div className="row">
                    <div className="col-md-6">
                        <ins>Messages</ins>
                        <div className="chat_box">
                            <ul>
                                {this.state.messages.map((msg, i) => (
                                    <li key={i}>
                                        <h2>{msg.senderName}</h2>
                                        <p>{msg.message}</p>
                                        <small>{Util.relativeTime(msg.time)}</small>
                                    </li>
                                ))}
                            </ul>
                        </div>
                        <form className="form-group" onSubmit={this.handleSubmit}>
                            <input type="text" className="form-control" placeholder="your message here..." value={this.state.data.message} name="message" onChange={this.handleChange}></input>
                            <button type="submit" className="btn btn-success">send</button>
                        </form>

                    </div>
                    <div className="col-md-6">
                        <ins>Users</ins>
                        <div className="chat_box">
                            <ul>
                                {this.state.users.map((user, i) => (
                                    <li key={i}>
                                        <button className="btn btn-default" onClick={() => this.selectUser(user)}>{user.name}</button>
                                    </li>
                                ))}
                            </ul>
                        </div>
                    </div>
                </div>


            </>
        )
    }
}
