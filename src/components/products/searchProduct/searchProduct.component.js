import React, { Component } from 'react'
import httpClient from '../../../util/httpClient';
import notificiation from '../../../util/notificiation';
import ViewProduct from '../viewProduct/viewProduct.component';
const defaultForm = {
    name: '',
    category: '',
    brand: '',
    minPrice: '',
    maxPrice: '',
    fromDate: '',
    toDate: '',
    multipleDateRange: false,
    tags: ''
}

export default class SearchProduct extends Component {
    constructor() {
        super();
        this.state = {
            allProducts: [],
            categories: [],
            names: [],
            searchResults: [],
            data: {

            },
            error: {

            },
            isSubmitting: false,
            isValidForm: false
        };
    }

    componentDidMount() {
        httpClient.POST('/product/search', {})
            .then(response => {
                const categories = [];
                (response.data || []).forEach(item => {
                    if (categories.indexOf(item.category) === -1) {
                        categories.push(item.category)
                    }
                });
                this.setState({
                    allProducts: response.data,
                    categories
                })
            })
            .catch(err => {
                notificiation.handleError(err);
            })

    }


    handleChange = e => {
        let { type, name, value, checked } = e.target;
        if (type === 'date') {
            console.log('value >>', value);
        }
        if (type === 'checkbox') {
            value = checked
        }
        if (name === 'category') {
            const names = this.state.allProducts.filter(item => item.category === value);
            this.setState({
                names
            })
        }

        this.setState(preState => ({
            data: {
                ...preState.data,
                [name]: value
            },
        }), () => {
            this.validateFrom(name);
        })
    }

    validateFrom(fieldName) {
        let errMsg;
        switch (fieldName) {
            case 'category':
                errMsg = this.state.data[fieldName]
                    ? ''
                    : 'Required Field*'
                break;
            default:
                break;
        }

        this.setState(preState => ({
            error: {
                ...preState.error,
                [fieldName]: errMsg
            }
        }), () => {
            const errors = Object
                .values(this.state.error)
                .filter(err => err);

            this.setState({
                isValidForm: errors.length === 0
            })
        })
    }

    handleSubmit = e => {
        e.preventDefault();
        this.setState({
            isSubmitting: true
        })
        const { data } = this.state;
        const requestData = { ...data };
        if (!requestData.multipleDateRange) {
            requestData.toDate = requestData.fromDate;
        }
        httpClient.POST('/product/search', requestData)
            .then(response => {
                if (!response.data.length) {
                    return notificiation.showInfo("No any product matched your search query")
                }
                this.setState({
                    searchResults: response.data
                })
            })
            .catch(err => {
                notificiation.handleError(err);
            })
            .finally(() => {
                this.setState({
                    isSubmitting: false
                })
            })
    }
    resetSearch = () => {
        this.setState({
            searchResults: [],
            data: {
                ...defaultForm
            }
        })
    }

    render() {
        let btn = this.state.isSubmitting
            ? <button disabled className="btn btn-info">submitting...</button>
            : <button disabled={!this.state.isValidForm} className="btn btn-primary" type="submit">Submit</button>

        let mainContent = this.state.searchResults.length
            ? <ViewProduct incomingData={this.state.searchResults} searchAgain={this.resetSearch}></ViewProduct>
            : <>
                <h2>Search Product</h2>
                <form onSubmit={this.handleSubmit} className="form-group">

                    <label>Category</label>
                    <select name="category" onChange={this.handleChange} className="form-control">
                        <option disabled selected>(Select Category)</option>
                        {this.state.categories.map((cat, i) => (
                            <option key={i} value={cat}>{cat}</option>
                        ))}

                    </select>
                    <p className="error">{this.state.error.category}</p>
                    {this.state.data.category && (
                        <>
                            <label>Name</label>
                            <select name="name" onChange={this.handleChange} className="form-control">
                                <option disabled selected>(Select Name)</option>
                                {this.state.names.map((name, i) => (
                                    <option key={i} value={name._id}>{name.name}</option>
                                ))}

                            </select>
                        </>
                    )}
                    <label>Brand</label>
                    <input type="text" className="form-control" placeholder="Brand" name="brand" value={this.state.data.brand} onChange={this.handleChange} ></input>
                    <label>Min Price</label>
                    <input type="number" className="form-control" placeholder="Min Price" name="minPrice" value={this.state.data.minPrice} onChange={this.handleChange} ></input>
                    <label>Max Price</label>
                    <input type="number" className="form-control" placeholder="Max Price" name="maxPrice" value={this.state.data.maxPrice} onChange={this.handleChange} ></input>
                    <label>Color</label>
                    <input type="text" className="form-control" placeholder="Color" name="color" value={this.state.data.color} onChange={this.handleChange} ></input>
                    <label>Tags</label>
                    <input type="text" className="form-control" placeholder="Tags" name="tags" onChange={this.handleChange} ></input>
                    <label>Select Date</label>
                    <input type="date" className="form-control" name="fromDate" onChange={this.handleChange} ></input>
                    <input type="checkbox" name="multipleDateRange" onChange={this.handleChange}></input>
                    <label>Multiple Date Range</label>
                    <br />
                    {
                        this.state.data.multipleDateRange && (
                            <>
                                <label>To Date</label>
                                <input type="date" className="form-control" name="toDate" onChange={this.handleChange} ></input>
                            </>
                        )
                    }
                    <input type="checkbox" name="discountedItem" checked={this.state.data.discountedItem} onChange={this.handleChange} ></input>
                    <label>Discounted Item</label>
                    <br />

                    {btn}
                </form>
            </>

        return (
            mainContent
        )
    }
}
