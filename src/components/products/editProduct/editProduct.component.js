import React, { Component } from 'react'
import httpClient from '../../../util/httpClient';
import notificiation from '../../../util/notificiation';
import { Loader } from '../../common/loader/loader.component';
import { ProductForm } from '../productForm/prouductForm.component';

export default class EditProduct extends Component {
    constructor() {
        super();
        this.state = {
            isLoading: true,
            product: {}
        };
    }

    componentDidMount() {
        this.setState({
            isLoading: true
        })
        this.productId = this.props.match.params['id'];
        httpClient.GET(`/product/${this.productId}`, true)
            .then(response => {
                this.setState({
                    product: response.data
                })
            })
            .catch(err => {
                notificiation.handleError(err);
            })
            .finally(() => {
                this.setState({
                    isLoading: false
                })
            })
    }

    edit = (data, files) => {
        httpClient.UPLOAD('PUT', `/product/${data._id}`, data, files)
            .then(response => {
                notificiation.showInfo('Product Updated');
                this.props.history.push('/view_product');
            })
            .catch(err => {
                notificiation.handleError(err);
            })
    }

    render() {
        let content = this.state.isLoading
            ? <Loader />
            : <ProductForm title="Edit Product" incomingData={this.state.product} submitCallback={this.edit} ></ProductForm>
        return (
            content
        )
    }
}
