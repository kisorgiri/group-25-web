import React, { Component } from 'react';
const defaultForm = {
    name: '',
    description: '',
    brand: '',
    category: '',
    price: '',
    color: '',
    manuDate: '',
    expiryDate: '',
    discountedItem: '',
    discountType: '',
    discountValue: '',
    tags: '',
    images: '',
}

const IMG_URL = process.env.REACT_APP_IMG_URL;

export class ProductForm extends Component {
    constructor() {
        super();
        this.state = {
            data: {
                ...defaultForm
            },
            error: {
                ...defaultForm
            },
            filesToUpload: [],
            isValidForm: false
        };
    }
    componentDidMount() {
        if (this.props.incomingData) {
            this.setState({
                data: {
                    ...defaultForm,
                    ...this.props.incomingData,
                    discountedItem: this.props.incomingData.discount ? this.props.incomingData.discount.discountedItem : '',
                    discountType: this.props.incomingData.discount ? this.props.incomingData.discount.discountType : '',
                    discountValue: this.props.incomingData.discount ? this.props.incomingData.discount.discountValue : '',

                }
            })
        }
    }

    handleChange = e => {
        let { type, name, value, checked, files } = e.target;
        if (type === 'checkbox') {
            value = checked
        }
        if (type === 'file') {
            const { filesToUpload } = this.state;
            filesToUpload.push(files[0])
            return this.setState({
                filesToUpload
            })
        }
        this.setState(preState => ({
            data: {
                ...preState.data,
                [name]: value
            }
        }), () => {
            this.validateFrom(name);
        })
    }

    validateFrom(fieldName) {
        let errMsg;
        switch (fieldName) {
            case 'category':
                errMsg = this.state.data[fieldName]
                    ? ''
                    : 'Required Field*'
                break;
            default:
                break;
        }

        this.setState(preState => ({
            error: {
                ...preState.error,
                [fieldName]: errMsg
            }
        }), () => {
            const errors = Object
                .values(this.state.error)
                .filter(err => err);

            this.setState({
                isValidForm: errors.length === 0
            })
        })
    }

    handleSubmit = e => {
        e.preventDefault();
        this.props.submitCallback(this.state.data, this.state.filesToUpload);
    }

    render() {
        let btn = this.props.isSubmitting
            ? <button disabled className="btn btn-info">submitting...</button>
            : <button disabled={!this.state.isValidForm} className="btn btn-primary" type="submit">Submit</button>

        let discountDetails = this.state.data.discountedItem
            ? <>
                <label>Disocount Type</label>
                <input type="text" className="form-control" placeholder="Disocount Type" name="discountType" value={this.state.data.discountType} onChange={this.handleChange} ></input>
                <label>Discount Value</label>
                <input type="text" className="form-control" placeholder="Discount Value" name="discountValue" value={this.state.data.discountValue} onChange={this.handleChange} ></input>
            </>
            : ''
        return (
            <>
                <h2>{this.props.title}</h2>
                <form onSubmit={this.handleSubmit} className="form-group">
                    <label>Name</label>
                    <input type="text" className="form-control" placeholder="Name" name="name" value={this.state.data.name} onChange={this.handleChange} ></input>
                    <label>Description</label>
                    <input type="text" className="form-control" placeholder="Description" name="description" value={this.state.data.description} onChange={this.handleChange} ></input>
                    <label>Category</label>
                    <input type="text" className="form-control" placeholder="Category" name="category" value={this.state.data.category} onChange={this.handleChange} ></input>
                    <p className="error">{this.state.error.category}</p>
                    <label>Brand</label>
                    <input type="text" className="form-control" placeholder="Brand" name="brand" value={this.state.data.brand} onChange={this.handleChange} ></input>
                    <label>Price</label>
                    <input type="number" className="form-control" placeholder="Price" name="price" value={this.state.data.price} onChange={this.handleChange} ></input>
                    <label>Color</label>
                    <input type="text" className="form-control" placeholder="Color" name="color" value={this.state.data.color} onChange={this.handleChange} ></input>
                    <label>Tags</label>
                    <input type="text" className="form-control" placeholder="Tags" name="tags" value={this.state.data.tags} onChange={this.handleChange} ></input>
                    <label>Manu Date</label>
                    <input type="date" className="form-control" name="manuDate" value={this.state.data.manuDate} onChange={this.handleChange} ></input>
                    <label>Expiry Date</label>
                    <input type="date" className="form-control" name="expiryDate" value={this.state.data.expiryDate} onChange={this.handleChange} ></input>
                    <input type="checkbox" name="discountedItem" checked={this.state.data.discountedItem} onChange={this.handleChange} ></input>
                    <label>Discounted Item</label>
                    <br />
                    {discountDetails}
                    <br />
                    {this.props.incomingData && (
                        <>
                            <label>Previous Image</label>
                            <img src={`${IMG_URL}/${this.state.data.images[0]}`} alt="productImg.jpg" width="600px"></img>
                            <br />
                        </>
                    )}
                    <label>Choose Image</label>
                    <input className="form-control" type="file" name="file" onChange={this.handleChange}></input>
                    <br />
                    {btn}
                </form>
            </>
        )
    }
}