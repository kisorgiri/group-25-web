import React, { Component } from 'react'
import { ProductForm } from '../productForm/prouductForm.component';
import httpClient from '../../../util/httpClient';
import notificiation from '../../../util/notificiation';

export default class AddProduct extends Component {

    constructor() {
        super();
        this.state = {
            isSubmitting: false
        };
    }

    add = (data, files) => {
        this.setState({
            isSubmitting: true
        })
        console.log('data is ', data);
        httpClient.UPLOAD('POST', '/product', data, files)
            .then(response => {
                notificiation.showSuccess('Product Added Successfully');
                this.props.history.push('/view_product');
            })
            .catch(err => {
                notificiation.handleError(err);
                this.setState({
                    isSubmitting: false
                })
            })
    }

    render() {
        return (
            <ProductForm title="Add Product" isSubmitting={this.state.isSubmitting} submitCallback={this.add}></ProductForm>
        )
    }
}
