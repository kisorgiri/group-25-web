import moment from 'moment';

function formatDate(date) {
    return moment(date).format('ddd YYYY-MM-DD');
}
function formatTime(date) {
    return moment(date).format('hh:mm a');
}

function relativeTime(date) {
    return moment(date).startOf('hour').fromNow();
}

export default {
    formatDate,
    formatTime,
    relativeTime
}