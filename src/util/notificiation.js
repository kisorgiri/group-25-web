import { toast } from 'react-toastify';

function showSuccess(msg) {
    toast.success(msg)
}

function showInfo(msg) {
    toast.info(msg);
}


function showWarnings(msg) {
    toast.warn(msg)
}

function handleError(error) {
    debugger;
    let errMsg = 'Something Went Wrong';
    let err = error.response;
    if (err && err.data) {
        errMsg = err.data.msg
    }
    toast.error(errMsg);
    // this function will act as a error handling function
    // whereever error appear in applicaton through to this function
    // step 1 check error
    // step 2 parse error
    // step 3 prepare error message
    // step 4 show then in UI
}



export default {
    showSuccess,
    showInfo,
    showWarnings,
    handleError
}