// web architecture
// MVC==> model view controller
// model and controller bidirectional communication


// flux architecture (state managment architecture)
// ==> unidirectional data flow
// actions ==> dispatcher==> store
// multiple store
// application logic reside on store

// redux // on top of flux (state management tool)
// ===> unidirectional data flow
// single store
// application logic reside on reducer
// actions===> reducer ====> store

// for redux implementation
 
// redux ==> state management tool
// react-redux ==> library==> it works as an glue to connect react and redux
// middlewares(thunk,saga) ==> delay dispatch